import boto3
import logging
import sys

logging_level = logging.INFO
logging.basicConfig(
    level=logging_level,
    format='[%(levelname)s] %(message)s',
    datefmt='%Y/%m/%d-%H:%M:%S'
)
logging.getLogger().setLevel(logging_level)

s3_client = boto3.client(service_name='s3', region_name='us-east-1')
paginator = s3_client.get_paginator("list_objects_v2")

bucket_name = sys.argv[1]

def encrypt_objects():
    objects = []
    page_iterator = paginator.paginate(Bucket=bucket_name)
    encrypt_object = 0
    
    for page in page_iterator:
        if 'Contents' not in page:
            continue
        objects += [item['Key'] for item in page['Contents']]

    for item in objects:
        head = s3_client.head_object(
            Bucket=bucket_name,
            Key = item
        )
        
        if 'ServerSideEncryption' in head:
            logging.info('Ojbect {} is already encrypted.'.format(item))
        
        else:
            logging.info("Encrypting object {}({} of {})".format(item,(objects.index(item)+1),len(objects)))
            response1 = s3_client.copy_object(
                Bucket=bucket_name,
                CopySource={'Bucket':bucket_name, 'Key':item},
                Key=item,
                ServerSideEncryption='AES256'
            )
            encrypt_object +=1
    logging.info("Job Completed Successfully. Encrypted {} of {} objects.".format(encrypt_object,len(objects)))
 
if __name__ == '__main__':
    encrypt_objects()
